import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Integer number;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        number = keyboard.nextInt();

        int i = 1;
        while (i <= number) {
            if (number %i == 0) {
                System.out.println(i);
            }
            i++;
        }
    }
}